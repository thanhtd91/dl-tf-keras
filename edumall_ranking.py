import tensorflow as tf
from functools import reduce
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from keras.utils import to_categorical
from sklearn.metrics import precision_recall_fscore_support
import warnings
warnings.filterwarnings('ignore')


data = pd.read_csv('data/data_combined_changed.csv')
data = data[data.price != 2019]
data = data.sort_values('created_at', ascending=True)


def tf_kron_prod(a, b):
    res = tf.einsum('ij,ik->ijk', a, b)
    res = tf.reshape(res, [-1, tf.reduce_prod(res.shape[1:])])
    return res


def tf_bin(x, cut_points, temperature=0.1):
    D = cut_points.get_shape().as_list()[0]
    W = tf.reshape(tf.linspace(1.0, D + 1.0, D + 1), [1, -1])
    cut_points = tf.contrib.framework.sort(cut_points)
    b = tf.cumsum(tf.concat([tf.constant(0.0, shape=[1]), -cut_points], 0))
    h = tf.matmul(x, W) + b
    res = tf.nn.softmax(h / temperature)
    return res


def tree_nn(x, cut_points_list, leaf_score, temperature=0.1):
    leaf = reduce(tf_kron_prod,
                  map(lambda z: tf_bin(x[:, z[0]:z[0] + 1], z[1], temperature), enumerate(cut_points_list)))
    return tf.matmul(leaf, leaf_score)


def count_word_address(data):
    data['address'] = data.address.apply(lambda x: str(x).replace(',,', ''))
    data['word_count_address'] = data.address.apply(lambda x: len([st for st in str(x).split(' ') if len(st) > 0]))
    return data


def remove_email(data):
    data.email = data.email.apply(lambda x: str(x).lower())
    email_group = data.groupby('email')['contact_id'].count().reset_index(name='amount').sort_values('amount',
                                                                                                     ascending=False)
    email_l8 = data.groupby('email')['l8'].mean().reset_index(name='percentage_l8')
    email_l8.percentage_l8 = email_l8.percentage_l8.apply(lambda x: x / data.shape[0])
    email_statistic = pd.merge(email_group, email_l8, on='email', how='left')
    email_removing = email_statistic.email.values[0:5]
    data = data[~data.email.isin(email_removing)]
    return data


data = remove_email(data)
data = count_word_address(data)

df_l8 = data[data.l8 == 1]
df_not_l8 = data[data.l8 < 1].iloc[0:df_l8.shape[0] + 1, :]
data = pd.concat([df_l8, df_not_l8], axis=0)
training = data.loc[:, ['created_at', 'sa', 'price', 'l8_c3_course_2w', 'tutin_l8_c3_course_2w',
                        'c3_course_buyed_1w', 'gdp_province', 'l8_c3_marketer_ratio',
                        'l8_c3_marketer_course', 'tutin_l8_c3_marketer',
                        'tutin_l8_c3_marketer_course', 'contact_id', 'l8', 'word_count_address']]
training['price_gdp'] = np.tanh(0.2 * (training.gdp_province / training.price))
training.loc[:, ['created_at', 'sa', 'l8_c3_course_2w', 'tutin_l8_c3_course_2w',
                 'l8_c3_marketer_ratio', 'price_gdp',
                 'l8_c3_marketer_course', 'tutin_l8_c3_marketer',
                 'tutin_l8_c3_marketer_course', 'contact_id', 'l8', 'word_count_address']].to_csv('data_changed.csv')
scaler = MinMaxScaler()
training.price = scaler.fit_transform(training.price.values.reshape(-1, 1))
training.gdp_province = scaler.fit_transform(training.gdp_province.values.reshape(-1, 1))
training.c3_course_buyed_1w = scaler.fit_transform(training.c3_course_buyed_1w.values.reshape(-1, 1))

X, y = training.loc[:,['sa','l8_c3_course_2w', 'tutin_l8_c3_course_2w','price_gdp',
                       'l8_c3_marketer_ratio', 'l8_c3_marketer_course','tutin_l8_c3_marketer',
                       'tutin_l8_c3_marketer_course', 'word_count_address']], to_categorical(training.l8, num_classes=2)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42, shuffle = True)

x_ph = tf.placeholder(tf.float32, [None, X.shape[-1]])
y_ph = tf.placeholder(tf.float32, [None, y.shape[-1]])

num_cut = [1 for _ in range(X.shape[-1])]
num_leaf = np.prod(np.array(num_cut) + 1)
num_class = 2

cut_points_list = [tf.Variable(tf.random_uniform([i])) for i in num_cut]
leaf_score = tf.Variable(tf.random_uniform([num_leaf, num_class]))

y_pred = tree_nn(x_ph, cut_points_list, leaf_score, temperature=0.1)
loss = tf.reduce_mean(tf.losses.softmax_cross_entropy(logits=y_pred, onehot_labels=y_ph))

correct_prediction = tf.equal(tf.argmax(y_pred, 1), tf.argmax(y_ph, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))
tf.summary.scalar('accuracy', accuracy)
merged = tf.summary.merge_all()
opt = tf.train.AdamOptimizer()
train_step = opt.minimize(loss)

sess = tf.InteractiveSession()

sess.run(tf.global_variables_initializer())
train_writer = tf.summary.FileWriter('./train', sess.graph)
test_writer = tf.summary.FileWriter('./test')

for i in range(2000):
    _, loss_e, summary = sess.run([train_step, loss, merged], feed_dict={x_ph: X_train, y_ph: y_train})
    train_writer.add_summary(summary, i)
    if i % 50 == 0:
        print(loss_e)
        acc_score, summary = sess.run([accuracy, merged], feed_dict={x_ph: X_test, y_ph: y_test})
        print('Accuracy %.2f' % acc_score)
        test_writer.add_summary(summary, i)

