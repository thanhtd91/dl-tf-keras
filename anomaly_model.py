import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.metrics import precision_recall_fscore_support
from keras.models import Sequential
from keras.layers import Dense
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.normalization import BatchNormalization
from keras.optimizers import Adam
from keras.utils import to_categorical
import tensorflow as tf
config = tf.ConfigProto()
config.gpu_options.allow_growth=True
sess = tf.Session(config=config)
data = pd.read_csv("new_training_anomaly.csv")
all_cols = ['sa', 'l8_c3_course_2w', 'tutin_l8_c3_course_2w', 'price_gdp',
       'l8_c3_marketer_ratio', 'l8_c3_marketer_course', 'tutin_l8_c3_marketer',
       'tutin_l8_c3_marketer_course', 'word_count_address']

X_train_anomaly, X_test_anomaly, y_train_anomaly, y_test_anomaly = train_test_split(data[all_cols],
                                                                                    data["is_anomaly"],
                                                                                    test_size=0.3)
num_inputs = X_train_anomaly.shape[-1]
y_train_anomaly_cat = to_categorical(y_train_anomaly)
y_test_anomaly_cat = to_categorical(y_test_anomaly)

model_anomaly = Sequential()
model_anomaly.add(Dense(64, input_dim=num_inputs))
model_anomaly.add(BatchNormalization())
model_anomaly.add(LeakyReLU())
model_anomaly.add(Dense(256))
model_anomaly.add(BatchNormalization())
model_anomaly.add(LeakyReLU())
model_anomaly.add(Dense(2, activation="sigmoid"))
optz = Adam()
model_anomaly.compile(loss="binary_crossentropy", optimizer=optz, metrics=["accuracy"])
model_anomaly.fit(X_train_anomaly, y_train_anomaly_cat,
                  validation_data=(X_test_anomaly, y_test_anomaly_cat),
                  epochs=500,
                  batch_size=1024,
                  verbose=1)

predictions = model_anomaly.predict(X_test_anomaly)
print(precision_recall_fscore_support(np.argmax(y_test_anomaly_cat, 1), np.argmax(predictions, 1)))

